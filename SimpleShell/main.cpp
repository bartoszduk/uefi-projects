#include <uefi.h>

BOOLEAN stringCompare(CHAR16 *str1, CHAR16 *str2)
{
	for (int i = 0; str1[i] != u'\0' && str2[i] != u'\0'; ++i)
	{
		if (str1[i] != str2[i])
		{
			return FALSE;
		}
	}

	return TRUE;
}

EFI_STATUS EFIAPI efi_main(
	IN EFI_HANDLE ImageHandle,
	IN EFI_SYSTEM_TABLE *SystemTable)
{
	SystemTable->ConOut->ClearScreen(SystemTable->ConOut);

	while (true)
	{
		SystemTable->ConOut->OutputString(SystemTable->ConOut, (CHAR16 *)u"SHELL >");

		EFI_INPUT_KEY key;
		CHAR16 input[256];
		UINT8 index = 0;

		while (true)
		{
			while (SystemTable->ConIn->ReadKeyStroke(SystemTable->ConIn, &key) == EFI_NOT_READY);

			if (key.ScanCode == 0)
			{
				if (key.UnicodeChar == u'\r' || key.UnicodeChar == u'\n')
				{
					if (stringCompare(input, (CHAR16 *)u"exit"))
					{
						return EFI_SUCCESS;
					}
					else if (stringCompare(input, (CHAR16 *)u"echo"))
					{
						SystemTable->ConOut->OutputString(SystemTable->ConOut, (CHAR16 *)u"\r\n");
						SystemTable->ConOut->OutputString(SystemTable->ConOut, (CHAR16 *)input[4]);
					}

					SystemTable->ConOut->OutputString(SystemTable->ConOut, (CHAR16 *)u"\r\n");
					break;
				}
				else
				{
					input[index] = key.UnicodeChar;
					CHAR16 output[2] = { key.UnicodeChar, u'\0' };
					SystemTable->ConOut->OutputString(SystemTable->ConOut, (CHAR16 *)output);
				}
			}
		}
	}
}