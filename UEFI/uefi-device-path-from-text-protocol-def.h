#pragma once

#include "uefi-base.h"
#include "uefi-device-path-protocol.h"

typedef
EFI_DEVICE_PATH_PROTOCOL*
(EFIAPI *EFI_DEVICE_PATH_FROM_TEXT_NODE) (
	IN CONST CHAR16* TextDeviceNode
	);

typedef
EFI_DEVICE_PATH_PROTOCOL*
(EFIAPI *EFI_DEVICE_PATH_FROM_TEXT_PATH) (
	IN CONST CHAR16* TextDevicePath
	);