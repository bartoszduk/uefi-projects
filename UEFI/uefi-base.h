#pragma once

#define CONST const

#define EFIAPI __cdecl
#define IN
#define OUT
#define OPTIONAL

typedef unsigned char BOOLEAN;

#define FALSE 0
#define TRUE 1

typedef __int64 INTN;
typedef unsigned __int64 UINTN;

typedef __int8 INT8;
typedef unsigned __int8 UINT8;

typedef __int16 INT16;
typedef unsigned __int16 UINT16;

typedef __int32 INT32;
typedef unsigned __int32 UINT32;

typedef __int64 INT64;
typedef unsigned __int64 UINT64;

/*typedef __int128 INT128;
typedef unsigned __int128 UINT128;*/

typedef char CHAR8;
typedef char16_t CHAR16;

typedef void VOID;

#define NATIVE_SIZE sizeof(void *)

typedef struct {
	UINT32 Data1;
	UINT16 Data2;
	UINT16 Data3;
	UINT8 Data4[8];
} EFI_GUID;

typedef UINTN EFI_STATUS;

typedef VOID *EFI_HANDLE;

/*
typedef UINT64 EFI_LBA;

typedef struct {
	UINT8 Addr[32];
} EFI_MAC_ADDRESS;

typedef struct {
	UINT8 Addr[4];
} EFI_IPv4_ADDRESS;

typedef struct {
	UINT8 Addr[16];
} EFI_IPv6_ADDRESS;

typedef union {
	UINT32 Addr[4];
	EFI_IPv4_ADDRESS v4;
	EFI_IPv6_ADDRESS v6;
} EFI_IP_ADDRESS;
*/

typedef struct {
	UINT64 Signature;
	UINT32 Revision;
	UINT32 HeaderSize;
	UINT32 CRC32;
	UINT32 Reserved;
} EFI_TABLE_HEADER;