#pragma once

#include "uefi-base.h"
#include "uefi-device-path-protocol.h"

typedef
CHAR16*
(EFIAPI *EFI_DEVICE_PATH_TO_TEXT_NODE) (
	IN CONST EFI_DEVICE_PATH_PROTOCOL* DeviceNode,
	IN BOOLEAN DisplayOnly,
	IN BOOLEAN AllowShortcuts
	);

typedef
CHAR16*
(EFIAPI *EFI_DEVICE_PATH_TO_TEXT_PATH) (
	IN CONST EFI_DEVICE_PATH_PROTOCOL *DevicePath,
	IN BOOLEAN DisplayOnly,
	IN BOOLEAN AllowShortcuts
	);