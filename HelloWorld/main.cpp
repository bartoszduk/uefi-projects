#include "uefi.h"
#include "uefi-graphics-output-protocol.h"

constexpr int rectangleWidth = 100;
constexpr int rectangleHeight = 100;
EFI_GRAPHICS_OUTPUT_BLT_PIXEL rectangle[rectangleWidth * rectangleHeight];

EFI_STATUS EFIAPI efi_main(
	IN EFI_HANDLE ImageHandle,
	IN EFI_SYSTEM_TABLE *SystemTable)
{
	SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_LIGHTGREEN);
	SystemTable->ConOut->ClearScreen(SystemTable->ConOut);

	SystemTable->ConOut->OutputString(SystemTable->ConOut, (CHAR16 *)u"Hello World! I'm UEFI...\r\n");

	EFI_GRAPHICS_OUTPUT_PROTOCOL *graphicsOutput;
	EFI_GUID graphicsOutputProtocolId = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;

	SystemTable->BootServices->HandleProtocol(SystemTable->ConsoleOutHandle, &graphicsOutputProtocolId, (VOID **)&graphicsOutput);
	
	for (int i = 0; i < rectangleHeight; ++i)
	{
		for (int j = 0; j < rectangleWidth; ++j)
		{
			rectangle[i * rectangleWidth + j] = { 255, 0, 0, 0 };
		}
	}

	graphicsOutput->Blt(graphicsOutput, rectangle, EFI_GRAPHICS_OUTPUT_BLT_OPERATION::EfiBltVideoFill, 0, 0, 200, 200, rectangleWidth, rectangleHeight, 0);

	SystemTable->ConIn->Reset(SystemTable->ConIn, FALSE);
	SystemTable->ConOut->EnableCursor(SystemTable->ConOut, TRUE);

	EFI_INPUT_KEY key;
	CHAR16 singleCharString[2] = {};
	CONST CHAR16 *output;

	while (true)
	{
		while (SystemTable->ConIn->ReadKeyStroke(SystemTable->ConIn, &key) == EFI_NOT_READY);

		if (key.ScanCode != 0)
		{
			output = u"\r\n - Huh, What was that? :D - \r\n";
		}
		else if (key.UnicodeChar == u'\r' || key.UnicodeChar == u'\n')
		{
			output = u"\r\n";
		}
		else if (key.UnicodeChar == u'\b')
		{
			output = u"\b \b";
		}
		else
		{
			singleCharString[0] = key.UnicodeChar;
			output = singleCharString;
		}

		SystemTable->ConOut->OutputString(SystemTable->ConOut, (CHAR16 *)output);
	}

	return EFI_SUCCESS;
}