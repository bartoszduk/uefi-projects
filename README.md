# UEFI Projects

This is collection of a few UEFI based projects:
- A "spiced up" HelloWorld program / bootloader
- A one person pong played against a simple ai (but there's no scoring at the moment)
- A small simple shell that only knows `echo` and `exit`

For informations on how to run these samples, look at the scripts in the project directories:
- `InstallToVM.bat`
- `MountVDisk.dp`
- `UnmountVDisk.dp`

Equivalently, these samples can be "installed" onto a real disk from which a device can then boot.

*These projects are meant to run without any operating system. They directly interact with the hardware via the UEFI protocols.*
